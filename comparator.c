#include "./comparator.h"
#include "./structs.h"

int rt_comparator(const void *v1, const void *v2)
{
    int l = ((struct Job *)v1)->runTime;
    int r = ((struct Job *)v2)->runTime;
    return (l - r);
}

int at_comparator(const void *v1, const void *v2)
{
    int l = ((struct Job *)v1)->arrivalTime;
    int r = ((struct Job *)v2)->arrivalTime;
    return (l - r);
}
