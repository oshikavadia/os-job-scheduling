#ifndef STRUCTS_H
#define STRUCTS_H

typedef enum {SJF,FIFO, STCF, RR1, RR2} schedulers;

struct Stats{
    char pid[9];
    int startTime;
    int endTime;
    int arrivalTime;
};

struct Job{
    char pid[9];
    int arrivalTime;
    int runTime;
};

#endif